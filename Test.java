package Projet;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import fr.unistra.pelican.ByteImage;
import fr.unistra.pelican.Image;
import fr.unistra.pelican.algorithms.io.ImageLoader;
import fr.unistra.pelican.algorithms.visualisation.Viewer2D;

public class Test {

	public static void main(String[] args) throws IOException {
		
		Image ImageRequete= ImageLoader.exec("images/motos/018.jpg");
		double[][] RequeteHisto = histoImage(ImageRequete);
		
		double[][][] LoadedBase = chargerBase("base");
		
		double[] tab = calculDistanceBase(RequeteHisto, LoadedBase);
		
		List<Double> list = extrairePhoto(tab);
		System.out.println(list);
		afficherPhoto(list, "motos");
		
		//double[][] histo = discretiserHisto(discretiserHisto((discretiserHisto(discretiserHisto(imageHistoColor(test))))));
		//HistogramTools.plotHistogram(histo[2]);
		
		//double[] histo2 = normaliserHisto(imageHistoColor(test)[1], 1000);
		//HistogramTools.plotHistogram(histo2);
		
		//Image[] base = loadBase(test, "motos", 283);
		//histoBase(debruiterBase(base));
		
		//double[][] tab = imageHistoColor(test);
		//discretiserHisto16(tab);

	}
	
	public static double[][] imageHistoColor(Image image) throws IOException{
		
		int largeur = image.getXDim();
		int longeur = image.getYDim();
		double[][] tab = new double[3][256];

		for(int i = 0; i < largeur; i++) {
			for(int j = 0; j <longeur; j++) {
				
				//ajout canal red
				tab[0][image.getPixelXYBByte(i, j, 0)]+=1;
				//ajout canal green
				tab[1][image.getPixelXYBByte(i, j, 1)]+=1;
				//ajout canal blue
				tab[2][image.getPixelXYBByte(i, j, 2)]+=1;
			}
		}
		
		//HistogramTools.plotHistogram(tab[0]);
		//HistogramTools.plotHistogram(tab[1]);
		//HistogramTools.plotHistogram(tab[2]);
		
		return tab;
	}
	
	public static double[][] discretiserHisto(double[][] tab) throws IOException{
		
		double size = Math.ceil(tab[0].length/2);
		double[][] histo = new double[3][(int)size];
		
		int j = 0;
		
		for(int i = 0; i < histo[0].length; i++) {
				histo[0][i] = tab[0][j]+tab[0][j+1];
				histo[1][i] = tab[1][j]+tab[1][j+1];
				histo[2][i] = tab[2][j]+tab[2][j+1];
				j +=2;
				
			}
		
		//HistogramTools.plotHistogram(histo[0]);
		//HistogramTools.plotHistogram(histo[1]);
		//HistogramTools.plotHistogram(histo[2]);
		
		return histo;
	}
	
	
	public static double[][] discretiserHisto16(double[][] tab) throws IOException{
		
		double[][] histo = discretiserHisto(discretiserHisto(discretiserHisto(discretiserHisto(tab))));
		//HistogramTools.plotHistogram(histo[1]);
		
		return histo;
	}
	
	
	public static double[] normaliserHisto(double[] tab, int pix) throws IOException{
			
			double[] histo = new double[tab.length];
			
			for(int i = 0; i < tab.length; i++) {
					histo[i]= tab[i]/pix;
				}
			
			return histo;
	}
	
	public static Image debruiterMedColor(Image image) {
		
		int largeur = image.getXDim();
		int longeur = image.getYDim();
		ByteImage image_color = new ByteImage(largeur, longeur, 1, 1, 3);
		
		List<Integer> listR = new ArrayList<Integer>(9);
		List<Integer> listG = new ArrayList<Integer>(9);
		List<Integer> listB = new ArrayList<Integer>(9);
		int medR, medG, medB;
		
		for(int i = 1; i < largeur-2 ; i++) {
			for(int j = 1; j <longeur-2; j++) {
				listR.clear();
				listR.add(image.getPixelXYBByte(i,j, 0));
				listR.add(image.getPixelXYBByte(i+1,j, 0));
				listR.add(image.getPixelXYBByte(i+1,j+1, 0));
				listR.add(image.getPixelXYBByte(i-1,j-1, 0));
				listR.add(image.getPixelXYBByte(i-1,j, 0));
				listR.add(image.getPixelXYBByte(i,j-1, 0));
				listR.add(image.getPixelXYBByte(i-1,j+1, 0));
				listR.add(image.getPixelXYBByte(i+1,j-1, 0));
				listR.add(image.getPixelXYBByte(i,j+1, 0));
				Collections.sort(listR);
				medR = listR.get(4);
				image_color.setPixelXYBByte(i,j, 0, medR);
				
				listG.clear();
				listG.add(image.getPixelXYBByte(i,j, 1));
				listG.add(image.getPixelXYBByte(i+1,j, 1));
				listG.add(image.getPixelXYBByte(i+1,j+1, 1));
				listG.add(image.getPixelXYBByte(i-1,j-1, 1));
				listG.add(image.getPixelXYBByte(i-1,j, 1));
				listG.add(image.getPixelXYBByte(i,j-1, 1));
				listG.add(image.getPixelXYBByte(i-1,j+1, 1));
				listG.add(image.getPixelXYBByte(i+1,j-1, 1));
				listG.add(image.getPixelXYBByte(i,j+1, 1));
				Collections.sort(listG);
				medG = listG.get(4);
				image_color.setPixelXYBByte(i,j, 1, medG);
				
				listB.clear();
				listB.add(image.getPixelXYBByte(i,j, 2));
				listB.add(image.getPixelXYBByte(i+1,j, 2));
				listB.add(image.getPixelXYBByte(i+1,j+1, 2));
				listB.add(image.getPixelXYBByte(i-1,j-1, 2));
				listB.add(image.getPixelXYBByte(i-1,j, 2));
				listB.add(image.getPixelXYBByte(i,j-1, 2));
				listB.add(image.getPixelXYBByte(i-1,j+1, 2));
				listB.add(image.getPixelXYBByte(i+1,j-1, 2));
				listB.add(image.getPixelXYBByte(i,j+1, 2));
				Collections.sort(listB);
				medB = listB.get(4);
				image_color.setPixelXYBByte(i,j, 2, medB);
			}
		}
		
		return image_color;
		}
	
	public static Image[] loadBase(String baseName, int nbImages){
		
		Image[] base = new Image[nbImages];
		
		for(int i = 0; i < 283; i++) {//a changer
			
			if(i < 10) {
				base[i] = ImageLoader.exec("images/"+baseName+"/00"+i+".jpg");
			}
			else if((i >= 10 )&&(i<100)) {
				base[i] = ImageLoader.exec("images/"+baseName+"/0"+i+".jpg");
			}
			else {
				base[i] = ImageLoader.exec("images/"+baseName+"/"+i+".jpg");
			}
			
		}
		
		//base[0].setColor(true);
		//Viewer2D.exec(base[0]);
		return base;
	}
	
	public static Image[] debruiterBase(Image[] base){
		
		for(int i = 0; i < 283; i++) {//a changer
			base[i] = debruiterMedColor(base[i]);
		}
		
		//base[0].setColor(true);
		//Viewer2D.exec(base[0]);
		return base;
	}
	
	public static double[][][] histoBase(Image[] base) throws IOException{
		
		double baseHisto[][][] = new double[283][3][16];//a changer
		double[][] disc = new double[3][16];
		
		for(int i = 0; i < 283; i++) {//a changer
			int nbPix = base[i].getXDim()*base[i].getYDim();
			disc = discretiserHisto16(imageHistoColor(base[i]));
			baseHisto[i][0] = normaliserHisto(disc[0], nbPix);
			baseHisto[i][1] = normaliserHisto(disc[1], nbPix);
			baseHisto[i][2] = normaliserHisto(disc[2], nbPix);
		}
		//HistogramTools.plotHistogram(baseHisto[0][2]);
		//HistogramTools.plotHistogram(baseHisto[9][2]);
		return baseHisto;
		}
	
	public static double[][] histoImage(Image image) throws IOException{
		
		double[][] histo = new double[3][16];
		
		for(int i = 0; i < 1; i++) {
			int nbPix = image.getXDim()*image.getYDim();
			histo = discretiserHisto16(imageHistoColor(image));
			histo[0] = normaliserHisto(histo[0], nbPix);
			histo[1] = normaliserHisto(histo[1], nbPix);
			histo[2] = normaliserHisto(histo[2], nbPix);	
		}
		//HistogramTools.plotHistogram(histo[2]);
		
		return histo;
		}
	
	public static double distanceHisto(double[][] histoRequete, double[][] histoBase) {
		
		double dist = 0;
		for(int i = 0; i < histoRequete[0].length; i++) {
			dist+= Math.pow(histoRequete[0][i]-histoBase[0][i],2);
			dist+= Math.pow(histoRequete[1][i]-histoBase[1][i],2);
			dist+= Math.pow(histoRequete[2][i]-histoBase[2][i],2);
		}
		return Math.sqrt(dist);
	}
	
	public static double[] calculDistanceBase(double[][] histoRequete, double[][][] histoBase) {
		
		double[] tab = new double[histoBase.length];
		int val;
		
		for(int i = 0; i < histoBase.length; i++) {
			tab[i] = distanceHisto(histoRequete, histoBase[i]);
		}
		
		return tab;
	}
	
	public static List<Double> extrairePhoto(double[] tab) {
		
		List<Double> list = new ArrayList<Double>(10);
		
		for(double i = 0; i < 10; i++) {
			double imin = -1;
			double min = 10000000;
			for(double j = 0; j < tab.length; j++) {
				
				if((min > tab[(int)j]) && (list.contains(j) == false)) {
					min = tab[(int)j];
					imin = j;
				}
			}
			list.add(imin);
		}
		return list;
	}
	
	public static void afficherPhoto(List<Double> tab, String base) {
		
		double index;
		String val;
		
		for(int i = 0; i < tab.size(); i++) {
			val = "";
			System.out.println(val);
			index = tab.get(i);
			if(index < 10) {
				val = "00" + index;
			}
			else if((index >= 10 )&&(index<100)) {
				val = "0" + index;
				
			}
			else {
				val = Double.toString(index);
			}
			
			val = val.substring(0, val.length() - 2);

			
			Image ImageResponse= ImageLoader.exec("images/"+base+"/"+val+".jpg");
			ImageResponse.setColor(true);
			Viewer2D.exec(ImageResponse);
			}

	}
	
	public static void sauvegarderBase(double[][][] base) {
	    try (FileOutputStream fichierSortie = new FileOutputStream("base.txt");
	         ObjectOutputStream objetSortie = new ObjectOutputStream(fichierSortie)) {
	        objetSortie.writeObject(base);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	public static double[][][] chargerBase(String nomFichier) {
	    double[][][] base = null;
	    try (FileInputStream fichierEntree = new FileInputStream("base.txt");
	         ObjectInputStream objetEntree = new ObjectInputStream(fichierEntree)) {
	        base = (double[][][]) objetEntree.readObject();
	    } catch (IOException | ClassNotFoundException e) {
	        e.printStackTrace();
	    }
	    return base;
	}
}


